'use strict';

/*
 * fizzBuzz
 *
 * Takes an input length and returns an array of the specified length
 * where the values are:
 *  1) the index + 1, eg: [ 1, 2 ], array[ 0 ] === 1, array[ 1 ] === 2
 *  2) values that would be multiples of 3 are replaced with 'fizz', eg: [ 1, 2, 'fizz' ]
 *  3) values that would be multiples of 5 are replaced with 'buzz', eg: [ 1, 2, 'fizz', 4, 'buzz' ]
 *  4) values that would be multiples of both 3 and 5 are replaced with 'fizzbuzz', eg: [ 1, 2, 'fizz', ... 14, 'fizzbuzz' ]
 *
 * Eg:
 *
 *   input: 10
 *   returns: [ 1, 2, 'fizz', 4, 'buzz', 'fizz', 7, 8, 'fizz', 'buzz' ]
 *
 */
 module.exports = ( length ) => {
   let getFizzBuzz = (number) => {

     if (number % 3 === 0 && number % 5 === 0) {
       return 'fizzbuzz';
     } else if (number % 3 === 0) {
       return 'fizz';
     } else if (number % 5 === 0) {
       return 'buzz';
     } else {
       return number;
     }
   };

   //Loop solution
   let arr = [];
   for (let i = 1; i <= length; i++){
     arr.push(getFizzBuzz(i));
   };
   return arr;


   //recursively solution
   /*

   let generateFizzBuzz = (accum, elementsRemaining) => {
     //base case
     if (elementsRemaining === 0) return [];
     accum = getFizzBuzz(elementsRemaining);
     return generateFizzBuzz(accum, elementsRemaining-1).concat(accum)
   }
   return generateFizzBuzz([],length);

   */
 };
