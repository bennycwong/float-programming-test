'use strict';

/*
 * deepFinder
 *
 * Takes an input and a test function and returns any values
 * in the input *recursively* that pass the test.
 *
 * Eg:
 *
 *   input: [ 'ant', 'baby', [ 'apple', 'banana', 'carrot' ], { foo: 'aardvark' }, 'allegory' ]
 *   test: value => /^a/i.test( value )
 *   returns: [ 'ant', 'apple', 'aardvark', 'allegory' ]
 *
 */
module.exports = ( input, test ) => {
    //if input is a single element
    if (typeof(input) !== 'object') {
      return test(input) ? [input] : []
    }
    //recursively flatten all elements
    let flatten = (arr) => {
      return arr.reduce(function(accumulator, elem) {
        if (typeof(elem) === 'object') {
          return accumulator.concat(
            flatten(Object.keys(elem).map((i) => elem[i]))
          );
        };
       return accumulator.concat(elem);
      }, []);
    }

    //filter out with the test
    return flatten(input).filter((value) => {
      return test(value)
    });

};
