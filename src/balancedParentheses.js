'use strict';

/*
 * balancedParentheses
 *
 * Takes an input string and returns true or false depending on if the string
 * has balanced parentheses.
 *
 * Eg:
 *
 *   input: '(x + y)'
 *   returns: true
 *
 *   input: '(x + y'
 *   returns: false
 *
 *   input: 'foo bar baz'
 *   returns: false
 *
 *   input: ''
 *   returns: false
 */
module.exports = ( input ) => {
  //In this problem set, the matching pair of the open is not needed,
  //But it would be more useful to have it if we want to extend it.
  let matchPair = {
    '(' : ')',
    ')' : '(',
  };

  let isOpenChar = (char) => {
    return char === "("
  }
  let isCloseChar= (char) => {
    return char === ")"
  }


  let isBalanced = (arr, stack) => {
    //base case: if you get to the end of the list and there is a stack, it's not balanced
    if (arr.length === 0) {
      return stack.length == 0; //if there is a stack, return false
    }
    let char = arr[0]; //helper to query for array
    //check to see if first elem is open character. If it is, push to stack.
    if (isOpenChar(char)) {
      stack.push(char);
    } else if (isCloseChar(char)) {
    // if it comes across a close char,
    // check to see if the close char has a matching pair
    // if it top of the stack and the matching pair of char are
    // not the same, then the string is unbalanced
      if (stack.pop() != matchPair[char]) return false;
    }
    //Otherwise, go to the next character
    return isBalanced(arr.slice(1), stack)
  }

  let inputArry = input.split('')
  //base case: if there is no open parentheses, it's not possible to be balanced
  if (inputArry.indexOf('(') === -1){
    return false;
  } else {
    return isBalanced(inputArry,[]);
  }
};
