'use strict';

/*
 * finder
 *
 * Takes an input and a test function and returns any values in
 * the input that pass the test.
 *
 * Eg:
 *
 *   input: [ 'ant', 'baby', [ 'apple', 'banana', 'carrot' ], { foo: 'aardvark' }, 'allegory' ]
 *   test: value => /^a/i.test( value )
 *   returns: [ 'ant', 'allegory' ]
 *
 */
module.exports = ( input, test ) => {
  if (typeof(input) !== 'object') {
    return test(input) ? [input] : []
  }
  return input.filter((value) => {
    // NOTE: usually (/^a/i.test( [ 'apple', 'banana', 'carrot' ] ) ) returns true
    // because it starts with an a.
    // therefore, I'm going to assume that the test functon should only operate on strings
    return test(value) && typeof(value) === 'string';
  });
};
